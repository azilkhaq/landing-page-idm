<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;

class NospamController extends BaseController
{
    public function index()
    {
        return view('nospam/index');
    }
}
