<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;

class SpeedupController extends BaseController
{
    public function index()
    {
        return view('speedup/index');
    }
}
