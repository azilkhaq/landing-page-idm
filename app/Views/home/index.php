<?= view('layouts/header'); ?>
<?= view('layouts/navbar'); ?>

<div class="banner-area shape-index transparent-nav content-double text-medium" id="home">
    <div class="box-table">
        <div class="box-cell">
            <div class="container">
                <div class="double-items thumb-140">
                    <div class="row align-center">
                        <div class="col-lg-6 left-info simple-video">
                            <div class="content" data-animation="animated fadeInUpBig">
                                <h1>PT Informasi <br> <span>Digital</span> Mandiri</h1>
                                <p>
                                    IDM, Informasi Digital Mandiri, was established in 2022 in Jakarta, Indonesia. We started as an IT service provider such as software development, Operation and system integration. We focusing in only IT area as our team members are Expert in Software Development.
                                </p>
                                <div class="button">
                                    <a class="btn circle btn-theme border btn-md" href="#">Get Started</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 right-info">
                            <img src="<?= base_url("public/assets/img/illustration/3.png") ?>" alt="Thumb">
                        </div>
                    </div>
                </div>
            </div>
            <div class="wavesshape">
                <img src="<?= base_url("public/assets/img/shape/1.png") ?>" alt="Shape">
            </div>
        </div>
    </div>
</div>

<div class="services-area bg-theme-small default-padding bottom-less">
    <div class="container">
        <div class="services-box text-center">
            <div class="row">
                <div class="col-lg-3 col-md-6 single-item">
                    <div class="item">
                        <i class="fas fa-bullseye"></i>
                        <h5>Objective</h5>
                        <p>
                            Meet customer requirements, by Serving Development and operation in IT Area with Good Quality, 24 hours 7 days .
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 single-item">
                    <div class="item">
                        <i class="fas fa-binoculars"></i>
                        <h5>Vision</h5>
                        <p>
                            Together move forward with our clients and pursue the best quality of services
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 single-item">
                    <div class="item">
                        <i class="fas fa-globe-asia"></i>
                        <h5>Core Values</h5>
                        <p>
                            Customer intimacy, Quality, and Delivery
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 single-item">
                    <div class="item">
                        <i class="fas fa-rocket"></i>
                        <h5>Mission</h5>
                        <p>
                            Talent Acquisition and Strengthen thru the nations with IT area as Domain.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="choseus-area default-padding" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="site-heading text-center">
                    <h2>Services</h2>
                </div>
            </div>
        </div>
        <div class="choseus-items">
            <div class="row">
                <div class="col-lg-6 thumb">
                    <img src="<?= base_url("public/assets/img/services.svg") ?>" alt="Thumb">
                </div>
                <div class="col-lg-6 info">
                    <div class="consultancy">
                        <h2>Consultancy</h2>
                        <p>
                            As our tagline to be closest with our clients, we proposing a new different way of IT Consultancy. Not only put the mainstream jargon, but also we give more value to our clients. Hence, our clients has value in more area.
                        </p>
                    </div>
                    <div class="system">
                        <h2>System Design, Devops</h2>
                        <p>
                            we offer computer system design, development and operation as Full-stack
                            domain. With our team members expertise in areas like Front Stack such Java Script technology, and Back end such Java, MySQL. We give more,
                            not only a piece of applications, but also beyond of applications itself,
                            like seam-less transfer knowledge, hand and legs extension, etc.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="work-process-area bg-gray default-padding" id="portofolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="site-heading text-center">
                    <h2>Portofolio</h2>
                </div>
            </div>
        </div>
        <div class="choseus-items">
            <div class="row">
                <div class="col-lg-6">
                    <div class="consultancy">
                        <h2>Advance SMPP asynchronous</h2>
                        <p>
                            With switch trend from P to P SMS to become A to P SMS, this area growth and required more
                            advance solution, in terms of service and agile delivery. Facebook,
                            Whatapps and up to domestic Partner require an Agile SMS platform with high throughput
                            and High continuity integration. We offer new models of SMPP method, with custom solution by adopt async process instead of normal way (sync process), this is give solution to overcome High latency issue
                        </p>
                    </div>
                    <div class="system">
                        <h2>Standardize IoT Gateway using MQTT</h2>
                        <p>
                            As Growth of IoT Business trends nowadays, our team members build a new way of IoT Device integration
                            with new model / methodology, while the normal way need normalize its protocols first in each of device, with our solution any device can connect to central / service gateway without normalize any protocols previously.
                        </p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img src="<?= base_url("public/assets/img/portofolio.svg") ?>" alt="Thumb">
                </div>
            </div>
        </div>
    </div>
</div>


<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>