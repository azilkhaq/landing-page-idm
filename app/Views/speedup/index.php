<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Game SpeedUp</title>
    <meta name="description" content="Bonx is a terrific esports website template with a slick and modern look.  It’s a robust gaming HTML template for bloggers and online gamers who want to share their enthusiasm for games on the internet." />
    <meta name="keywords" content="	bootstrap, clean, esports, game, game portal, Game website, gamer, games, gaming, magazine, match, modern, online game, sport, sports">
    <meta name="author" content="Code Carnival">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= base_url("public/speedup/img/bg/logo2.png") ?>" type="image/png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Exo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Metal+Mania&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("public/speedup/css/vendor/bootstrap.min.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/speedup/css/slick.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/speedup/css/icofont.min.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/speedup/css/animate.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/speedup/css/nice-select.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/speedup/css/magnific-popup.css") ?>">
    <link rel="stylesheet" href="<?= base_url("public/speedup/css/style.css") ?>">
</head>

<header class="header_section header_transparent sticky-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main_header d-flex justify-content-between align-items-center">
                    <!--main menu start-->
                    <div class="main_menu d-none d-lg-block">

                    </div>
                    <!--main menu end-->
                    <div class="header_right_sidebar d-flex align-items-center">
                        <div class="header_logo">
                            <a class="sticky_none" href="index.html"><img aria-label="logo" width="215" height="79" src="<?= base_url("public/speedup/img/bg/logo.png") ?>" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="page_wrapper">
    <section class="hero_banner_section d-flex align-items-center mb-130">
        <div class="container">
            <div class="hero_banner_inner">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="hero_content">
                            <h1 class="wow fadeInUp" data-wow-delay="0.1s" data-wow-duration="1.1s">Game <br>
                                SpeedUp</h1>
                            <p style="font-size: 20px; margin-bottom: 0px;" class="wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.2s">Sebuah Revolusi kenyamanan bermain game di era tuntutan</p>
                            <p style="font-size: 20px; margin-bottom: 0px;" class="wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.2s">Anti nge-Lag! Didukung penuh oleh Jaringan Berkualitas dan Terbaik dari </p>
                            <p style="font-size: 20px;" class="wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.2s">Telkomsel dari Sabang sampai Merauke.</p>

                            <p style="font-size: 20px; margin-bottom: 0px;" class="wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.2s">Game SpeedUp is more than just a booster! yang</p>
                            <p style="font-size: 20px; margin-bottom: 0px;" class="wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.2s">Sepenuhnya kami dedikasikan untuk anda semua </p>
                            <p style="font-size: 20px;" class="wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1.2s">Gammer di seluruh Indonesia!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero_position_img">
            <img width="926" height="772" src="<?= base_url("public/speedup/img/bg/hero-position-img.webp") ?>" alt="">
        </div>
    </section>
</div>

<script src="<?= base_url("public/speedup/js/vendor/modernizr-3.7.1.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/vendor/jquery-3.6.0.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/vendor/jquery-migrate-3.3.2.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/vendor/popper.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/vendor/bootstrap.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/slick.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/wow.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/jquery.nice-select.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/jquery.magnific-popup.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/jquery.counterup.min.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/jquery-waypoints.js") ?>"></script>
<script src="<?= base_url("public/speedup/js/main.js") ?>"></script>