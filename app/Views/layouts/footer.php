<footer class="bg-gray">
    <div class="container">
        <!-- Start Footer Bottom -->
        <div class="footer-bottom">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <p>&copy; Copyright 2022. <a href="<?= base_url() ?>">PT Informasi Digital Mandiri</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Footer Bottom -->
    </div>
</footer>