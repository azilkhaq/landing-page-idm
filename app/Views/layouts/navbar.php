<header id="home">
    <nav class="navbar navbar-default navbar-fixed dark no-background bootsnav">

        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="<?= base_url('/') ?>">
                    <img src="<?= base_url("public/assets/img/logo.png") ?>" class="logo logo-display" alt="Logo">
                    <img src="<?= base_url("public/assets/img/logo.png") ?>" class="logo logo-scrolled" alt="Logo">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="#" data-out="#">
                    <li>
                        <a class="smooth-menu" href="<?= base_url('/') ?>">Home</a>
                    </li>
                    <li>
                        <a class="smooth-menu" href="#services">Services</a>
                    </li>
                    <li>
                        <a class="smooth-menu" href="#portofolio">Portofolio</a>
                    </li>
                    <li>
                        <a class="smooth-menu" href="<?= base_url('speedup') ?>">Game SpeedUp</a>
                    </li>
                    <li>
                        <a class="smooth-menu" href="<?= base_url('nospam') ?>">Nospam</a>
                    </li>
                </ul>
            </div>
        </div>

    </nav>
</header>