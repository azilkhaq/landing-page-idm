<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IDM | Informasi Digital Mandiri">
    <title>IDM | Informasi Digital Mandiri</title>
    <link rel="shortcut icon" href="<?= base_url("public/assets/img/logo.png") ?>" type="image/x-icon">
    <link href="<?= base_url("public/assets/css/bootstrap.min.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/font-awesome.min.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/flaticon-set.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/magnific-popup.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/owl.carousel.min.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/owl.theme.default.min.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/animate.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/bootsnav.css")?>" rel="stylesheet" />
    <link href="<?= base_url("public/assets/css/style.css")?>" rel="stylesheet">
    <link href="<?= base_url("public/assets/css/responsive.css")?>" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">

</head>